#define _GNU_SOURCE
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>

#include "read_line.h"

#define BLOCK_SIZE (64*1024)

#define MAX_PATH_LENGTH 1024

//#define BUF_SIZE (2*1024*1024)   /* 2MiB */
#define BUF_SIZE (23*64*1024)    /* 1507328B =1.4MiB > 150B * N_LINES_FLUSH */

//#define READ_BUF_SIZE    (512*1024*1024)   /* 512MiB */
#define READ_BUF_SIZE    (1024*1024*1024)   /* 1GiB */
//#define READ_BLOCK_SIZE   (16*1024*1024)   /* 16MiB */
#define READ_BLOCK_SIZE   (8*1024*1024)   /* 8MiB */

#define N_LINES_FLUSH 10000


typedef struct {
	int fd;
	char *buf;
	char *cur;
	size_t buf_size;
	size_t size;
	size_t total;
} buf_data_t;


static int get_line(rl_file_t *rlf, buf_data_t *bd)
{
	ssize_t n = read_line(rlf, bd->cur, bd->buf_size);
	if (n >= 0) {
		bd->size += n;
		bd->cur += n;
		bd->buf_size -= n;
	}
	return n;
}


static void write_files(int n, buf_data_t *buf_data, int is_last)
{
#pragma omp parallel for schedule(dynamic,1)
	for (int i = 0; i < n; i++) {
		buf_data_t *bd = &buf_data[i];
		size_t size = (bd->size / BLOCK_SIZE) * BLOCK_SIZE;
		if (!is_last) {
			if (size > 0) {
				ssize_t n_write = write(bd->fd, bd->buf, size);
				if (n_write < 0) {
					perror("write");
					exit(1);
				}
				size_t rest = bd->size - size;
				bd->total += size;
				bd->size = rest;
				if (rest > 0) memcpy(bd->buf, bd->buf+size, rest);
				bd->cur = bd->buf + rest;
				bd->buf_size = BUF_SIZE - rest;
			}
		} else {
			if (bd->size > size) size += BLOCK_SIZE;
			ssize_t n_write = write(bd->fd, bd->buf, size);
			if (n_write < 0) {
				perror("write");
				exit(1);
			}
			bd->total += bd->size;
			ftruncate(bd->fd, bd->total);
		}
	}
}


int main(int argc, char *argv[])
{
	if (argc != 4) {
		fprintf(stderr, "usage: %s n file format\n", argv[0]);
		exit(1);
	}

	int n = atoi(argv[1]);
	char *file = argv[2];
	char *format = argv[3];

	buf_data_t *buf_data = (buf_data_t*)malloc(sizeof(buf_data_t) * n);

	for (int i = 0; i < n; i++) {
		char file_name[MAX_PATH_LENGTH];
		snprintf(file_name, MAX_PATH_LENGTH, format, i);
		buf_data[i].fd = open(file_name, O_WRONLY | O_CREAT | O_TRUNC | O_DIRECT, 00600);
		if (buf_data[i].fd < 0) {
			perror(file_name);
			exit(1);
		}
		assert(BUF_SIZE % BLOCK_SIZE == 0);
		int ret = posix_memalign((void *)&buf_data[i].buf, BLOCK_SIZE, BUF_SIZE);
		if (ret != 0) {
			fprintf(stderr, "posix_memalign failure: %d\n", ret);
			exit(1);
		}
		buf_data[i].cur = buf_data[i].buf;
		buf_data[i].buf_size = BUF_SIZE;
		buf_data[i].size = 0;
		buf_data[i].total = 0;
	}

	rl_file_t* rlf = read_line_init(file, READ_BUF_SIZE, READ_BLOCK_SIZE);

	size_t n_lines_flush = N_LINES_FLUSH * (size_t)n;
	size_t n_lines = 0;

	for (;;) {
		int f = (n_lines / 4) % n;
		ssize_t n_read = get_line(rlf, &buf_data[f]);
		n_lines++;

		if (n_read <= 0) {
			write_files(n, buf_data, 1);
			break;
		}

		if (n_lines % n_lines_flush == 0) {
			write_files(n, buf_data, 0);
		}
		if (n_read <= 0) break;
	}

	for (int i = 0; i < n; i++) {
		close(buf_data[i].fd);
		free(buf_data[i].buf);
	}
	free(buf_data);

	return 0;
}
