#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <unistd.h>
#include <assert.h>

#include "read_line.h"


rl_file_t* read_line_init(char *file, size_t buf_size, size_t read_size)
{
	assert(buf_size % read_size == 0);
	rl_file_t *rlf = (rl_file_t*)malloc(sizeof(rl_file_t));
	rlf->fd = open(file, O_RDONLY, 0);
	if (rlf->fd < 0) {
		perror(file);
		exit(1);
	}
	rlf->file_offset = 0;
	rlf->buffer = (char*)malloc(buf_size);
	rlf->buf_size = buf_size;
	rlf->read_size = read_size;
	rlf->n_read = 0;
	rlf->cur = rlf->buffer;

	return rlf;
}


static ssize_t read_to_buffer(rl_file_t *rlf)
{
	size_t n_read = 0;

#pragma omp parallel reduction(+:n_read)
	{
#pragma omp for schedule(dynamic,1)
	for (off_t o = 0; o < rlf->buf_size; o += rlf->read_size) {
		ssize_t n = pread(rlf->fd, rlf->buffer+o, rlf->read_size, rlf->file_offset+o);
		assert(n >= 0);
		n_read += n;
	}
	}

	rlf->file_offset += n_read;
	rlf->n_read = n_read;
	rlf->cur = rlf->buffer;
#ifdef VERBOSE
	fprintf(stderr, "%.2f GiB\n", (double)rlf->file_offset/1024/1024/1024);
#endif

	return n_read;
}


ssize_t read_line(rl_file_t* rlf, char *buffer, size_t buf_size)
{
	ssize_t n = rlf->n_read - (rlf->cur - rlf->buffer);
	if (n <= 0) read_to_buffer(rlf);
	if (rlf->n_read == 0) return 0;

	char *q = memchr(rlf->cur, '\n', n);
	if (q != NULL) {
		q++;
		size_t m = q - rlf->cur;
		assert(buf_size >= m);
		memcpy(buffer, rlf->cur, m);
		rlf->cur = q;
		return m;
	} else {
		assert(buf_size >= n);
		memcpy(buffer, rlf->cur, n);
		rlf->cur += n;
		size_t m = n;
		m += read_line(rlf, buffer+n, buf_size-n);
		return m;
	}
}


void dump_buffer(rl_file_t *rlf)
{
	write(1, rlf->buffer, rlf->n_read);
}


void dump_file(rl_file_t *rlf)
{
	ssize_t n;
	ssize_t total = 0;
	while ((n = read_to_buffer(rlf)) > 0) {
		dump_buffer(rlf);
		total += n;
		fprintf(stderr, "%ld\n", total);
	}
}
