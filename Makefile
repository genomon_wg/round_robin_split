#CC = gcc
#CFLAGS = -g -Wall -fopenmp
CC = fccpx
CFLAGS = -Nclang -Ofast -fopenmp

#CFLAGS +=-DVERBOSE

PROGRAM = round_robin_split

OBJS = read_line.o round_robin_split.o

all: $(PROGRAM)

$(PROGRAM): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $@

.c.o:
	$(CC) $(CFLAGS) -c $<

.PHONY: test
test: $(PROGRAM)
	cd test && ./test.sh

clean:
	rm -f $(OBJS)

distclean: clean
	rm -f $(PROGRAM)
	rm -f test/out/*

round_robin_split.o read_line.o: read_line.h 
