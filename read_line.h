#ifndef READ_LINE_H
#define READ_LINE_H

#include <unistd.h>

typedef struct {
	int fd;
	off_t file_offset;
	char *buffer;
	size_t buf_size;
	size_t read_size;
	size_t n_read;
	char *cur;
} rl_file_t;


rl_file_t* read_line_init(char *file, size_t buf_size, size_t read_size);

ssize_t read_line(rl_file_t* rlf, char *buffer, size_t buf_size);

void dump_buffer(rl_file_t *rlf);

void dump_file(rl_file_t *rlf);

#endif /* READ_LIEN_H */
