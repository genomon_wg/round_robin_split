#!/bin/sh

rm -f out/*

export OMP_NUM_THREADS=4

../round_robin_split 3 data/in.txt out/test_%04d

if diff -q -x .gitignore -r out data/out
then
        echo "OK"
else
        echo "NG"
fi
