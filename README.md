# round_robin_split

ラウンドロビンシャッフルによるfastqファイル分割プログラム。

## Usage

    round_robin_split <n> <input_fastq> <output_format> <num_threads>

output_formatには、分割後のファイル名をprintf等と同様なフォーマット記法を用いて指定する。

### Example

    export OMP_NUM_THREADS=12
    round_robin_split 100 input.fastq output_%03d.fastq

この例では、入力input.fastqを、output_000.fastqからoutput_099.fastqまでの100ファイルに分割する。
入出力には12スレッドを使用する。

## 京/富岳での同時オープン可能なファイル数の制限について

ディフォルトの上限は1024ファイル。
それ以上のファイル数を同時オープンするためには、実行ジョブスクリプトに

    #PJM --rsc-list "proc-openfd=32768"

などと指定する。この例では、32768ファイルまで同時オープン可能。
